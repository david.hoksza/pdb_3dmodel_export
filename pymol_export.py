import argparse
import os
import sys

import pymol
from pymol import cmd



def open_pymol(args):

    # pymol environment
    dir = os.getcwd()
    sys.path.insert(0, dir)
    os.environ['PYMOL_PATH'] = os.path.join(dir, 'pymol/pymol_path')

    import __main__

    if args.gui:
        params = '-qi'
    else:
        params = '-c'
    __main__.pymol_argv = ['pymol', params]



    # Call the function below before using any PyMOL modules.
    pymol.finish_launching()


def export(args):

    obj_name = 'main'

    cmd.delete('all')
    cmd.set('use_shaders', 'off')
    cmd.set('internal_gui', 'on')
    cmd.load(args.input, obj_name)

    if args.visuals:
        for v in args.visuals.split(','):
            v_name = 'derived_{}'.format(v.strip())
            cmd.copy(v_name, obj_name)
            cmd.show_as(v.strip(), v_name)
        cmd.delete('main')

    if args.settings:
        for kv in args.settings.split(','):
            [k,v] = kv.split('=')
            cmd.set(k.strip(),v.strip())

    format = args.output_format if args.output_format else None
    cmd.save(args.output, format=format)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input",
                        required=True,
                        help="Path to the input PDB file")
    parser.add_argument("-o", "--output",
                        required=False,
                        help="Path to the output model file")
    parser.add_argument("-of", "--output-format",
                        required=False,
                        help="[dae|obj|pov]")
    parser.add_argument("-v", "--visuals",
                        required=False,
                        help="Comma-delimited list of ")
    parser.add_argument("-s", "--settings",
                        required=False,
                        help="Comma-delimited key-value pairs in the form key1=val1,key2=val2")
    parser.add_argument("--gui",
                        required=False,
                        action='store_true',
                        default=False,
                        help="Whether PyMOL GUI should be opened")

    args = parser.parse_args()

    open_pymol(args)
    export(args)

