# Exporting PDB files into 3D models

This projects aims at providing scripts facilitating:

- export of molecular structure files (currently focused on [PDB](https://en.wikipedia.org/wiki/Protein_Data_Bank_(file_format))) into different 3D model formats
- processing of the 3D model files

## Export to 3D models

### PyMOL

The ```pymol_export.py``` script converts an input PDB file into an output 3D model
file. The scripts takes input PDB file, reads the structure, creates required visual
representations of the structures, sets provided settings and renders the result
into the target format. It should be noted that the script treats the 
input structure as a whole and (currently) does not support visualization of different
selections using different representations. It also does not support other more complex 
operations (such as labeling) which are available in PyMOL.

**Warning**: In order for the script to be functional, PyMOL needs to be accessible via the 
``pymol`` command from the command line.  

```python pymol_export.py -h``` lists the available parameters:
- Path to the input PDB file (can be also a relative path)
- Path to the output file to store the resulting model. There are 
[multiple types](https://pymolwiki.org/index.php/Save) 
of output types supported by PyMOL and the type is autodetected from the 
extension of the output path. For the purpose of 3D modelling, 
the relevant extensions are ``obj``, ``dae`` and possibly ``pov``.
- The output type can also be explicitly specified by the ``output-format``
parameter
- The ``visuals`` parameter is to be used to pass the list of visual 
representations for the structure (if none provided, the default visualization
will be used). The list of PyMOL-supported 
representations can be found [here](https://pymolwiki.org/index.php/Show_as). Most
commonly used are ``line``, ``spheres`` , ``cartoon``, ``surface``
- The ``settings`` parameter can be used to pass comma-delimited key=value pairs
of settings. There exists 
[a plethora of settings](https://pymolwiki.org/index.php/Category:Settings) 
that can be used to customize the visualization. Regarding quality
of the resulting models, the most relevant are probably:

    - [surface quality](https://pymolwiki.org/index.php/Surface_quality)
        - int (in practice typical values are 1, 2 and 3)  
    - [sphere quality](https://pymolwiki.org/index.php/Sphere_quality)
        - int (default 1) [-4;4]
        - controls the rendering quality of sphere objects
        - controls how many triangles are used to render the surface. 
        Surface qualities of 2 and above are only practical for very small systems. 
        Surface qualities below 2 are only practical for large globular systems ([source](https://pymol.org/dokuwiki/doku.php?id=setting:surface))                
    - [stick quality](https://pymol.org/dokuwiki/doku.php?id=setting:stick_quality)
        - int        
        - This setting does not seem to have effect on export to WRL.
    - [cartoon sampling](https://pymolwiki.org/index.php/Cartoon_sampling)
        - int (default 10)
        - the number of segments that makes up a given length of cartoon
        
    Other relevant settings can be found in the following:
    - [sticks](https://pymolwiki.org/index.php/Sticks)
    - [surface](https://pymol.org/dokuwiki/doku.php?id=setting:surface)        
    - [ray settings](https://pymol.org/dokuwiki/doku.php?id=setting:ray)
    
- gui switch causes the conversion to be run with the PyMOL GUI opened. After
the conversion, the GUI stays opened allowing thus the user to see the structure
in the form in which it was rendered. 
    
A simple example of converting an input PDB file into an output obj 
containing both cartoon and line representations with decreased rendering
quality of the cartoon representation:

````commandline
python pymol_export.py --input test/2n0a.pdb --output test/2n0a.obj --visuals "cartoon,lines" --settings "cartoon_sampling=0" --gui
````
